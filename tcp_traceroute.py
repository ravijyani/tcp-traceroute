#! /usr/bin/env python3

import argparse
import subprocess
import os
import socket
from threading import Thread
import sys 
from scapy.layers.inet import *
from scapy.layers.dns import *
import time
import traceback


def run(args):
    max_hops = args.m
    dst_port = args.p
    target = args.t
    sport_list = [12121]
    sequence_no = 0

    try:
        target_ip = socket.gethostbyname(target).strip(" ")
    except Exception as e:
        print("Target domain name '{}' not found".format(target))
        exit_tcp_traceroute(str(e))

    print("traceroute to {} ({}), {} hops max, TCP SYN to port {}".format(target, target_ip, max_hops, dst_port))
    traceroute(max_hops, dst_port, target_ip, sport_list, sequence_no)


def main():
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("-m", help="Max hops to probe (default = 30)", dest="m", metavar="MAX_HOPS", type=int, required=False, default=30)
    parser.add_argument("-p", help="TCP destination port (default = 80)", dest="p", metavar="DST_PORT", type=int, required=False, default=80)
    parser.add_argument("-t", help="Target domain or IP",dest="t", metavar="TARGET", type=str, required=True, default="1.1.1.1")
    parser.set_defaults(func=run)
    args = parser.parse_args()
    args.func(args)


def traceroute(max_hops: int, dst_port: int, target: str, sport_list: list, sequence_no: int):
    tcp_socket = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(ETH_P_IP))
    tcp_socket.bind(('ens3', 0))
    tcp_socket.setblocking(0)
    hop_num = 1
    sleep_time = 0.05
    timeout = 2

    while hop_num <= max_hops:
        raw_tcp_packet = Ether()/IP(dst=target, ttl=hop_num)/TCP(dport=dst_port, sport=sport_list[0], flags="S", seq=sequence_no)        
        last_hop_ip = None 
        hop_details_string = ""
        
        for i in range(3):
            timer = 0
            start_time = time.time()
            tcp_socket.send(bytes(raw_tcp_packet))
            while True:
                try:
                    recv_pkt = tcp_socket.recv(1480)
                    end_time = time.time()
                    ether_pkt = Ether(recv_pkt)
                    if ICMP in ether_pkt:
                        if ether_pkt['IP in ICMP'].dst == target and ether_pkt['TCP in ICMP'].sport == sport_list[0] and ether_pkt['TCP in ICMP'].flags == "S" and ether_pkt['TCP in ICMP'].ack == sequence_no and ether_pkt[ICMP].type == 11:
                            last_hop_ip, hop_details_string = print_hop_details(ether_pkt, hop_num, start_time, end_time, last_hop_ip, i, hop_details_string, False)
                            break
                    if TCP in ether_pkt:
                        if ether_pkt[IP].src == target and ether_pkt[TCP].dport == sport_list[0] and ether_pkt[TCP].flags == "SA" and ether_pkt[TCP].ack == sequence_no+1:
                            last_hop_ip, hop_details_string = print_hop_details(ether_pkt, hop_num, start_time, end_time, last_hop_ip, i, hop_details_string, False)
                            hop_num = max_hops 
                            break   
                    timer += sleep_time
                    if timer > timeout:
                        last_hop_ip, hop_details_string = print_hop_details(ether_pkt, hop_num, start_time, end_time, last_hop_ip, i, hop_details_string, True)
                        break
                except BlockingIOError as e:
                    pass
                except Exception as e:
                    print("Error occured while receiving packet: {}".format(str(e)))
                    exit_tcp_traceroute(str(e))

        hop_num = hop_num + 1


def print_hop_details (ether_pkt: Ether, hop_num: int, start_time: time, end_time: time, last_hop_ip: str, loop_no: int, hop_details_string: str, timeout_flag: bool):
    
    if not timeout_flag:
        hop_ip = ether_pkt[IP].src
        hop_latency = round((end_time-start_time)*1000, 2)
        try:
            hop_domain_name = socket.gethostbyaddr(hop_ip)[0]
        except Exception as e:
            hop_domain_name = hop_ip
        if last_hop_ip != hop_ip:
            hop_details_string += " {} ({}) {} ms".format(hop_domain_name, hop_ip, hop_latency)
        else:
            hop_details_string += " {} ms".format(hop_latency)
    else:
        hop_ip = last_hop_ip
        hop_details_string += " *"

    if loop_no == 0:
        hop_details_string = "{:>2} ".format(hop_num) + hop_details_string
    elif loop_no == 2:
        print(hop_details_string)

    return hop_ip, hop_details_string


def exit_tcp_traceroute(message: str):
    print(message)
    print("Exiting TCP Traceroute. \nPlease Try agian later")
    exit(0)


if __name__=="__main__":
    main()
